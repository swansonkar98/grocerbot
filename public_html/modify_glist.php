<?php
session_start();

if (!isset($_SESSION['logged_in']) || !($_SESSION['logged_in'])) {
    header("Location: https://grocer.bot.nu/login.php");
    //header("Location: http://localhost/public_html/index.php");
}

require_once("../resources/config.php");

// Create connection
$conn = new mysqli($config['host'], $config['username'], $config['password'], $config['dbname']);

// Check connection
if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
}

$userEmail = $_SESSION['email'];

// Deletes a list by id
if (isset($_POST['del_glist'])) {
    $glistIDSelected = $_POST['glist_sel'];
    $sql1 = "DELETE FROM glist WHERE glist_id = $glistIDSelected";
    if ($conn->query($sql1) === TRUE) {
        // Redirect to index.php
        header("Location: https://grocer.bot.nu");
        //header("Location: http://localhost/public_html/index.php");
    } else {
        echo "Error: " . $sql2 . "<br>" . $conn->error;
    }
}

// Creates a list by name
if (isset($_POST['glist_name'])) {
    $glistName = $_POST['glist_name'];
    $sql2 = "INSERT INTO glist (name) VALUES ('$glistName')";
    $sql3 = "INSERT INTO user_to_glist (user_id, list_id) VALUES ((SELECT user.user_id FROM user WHERE email = '$userEmail'), LAST_INSERT_ID())";
    if ($conn->query($sql2) === TRUE) {
        if ($conn->query($sql3) === TRUE) {
        // Redirect to index.php
        header("Location: https://grocer.bot.nu");
        //header("Location: http://localhost/public_html/index.php");
        } else {
        echo "Error: " . $sql3 . "<br>" . $conn->error;
        }
    } else {
        echo "Error: " . $sql2 . "<br>" . $conn->error;
    }
}

// Deletes a food item by id in list
if (isset($_POST['remove_mapped_item'])) {
    $mappedItemID = $_POST['remove_mapped_item'];
    $sql4 = "DELETE FROM item_to_glist WHERE id = $mappedItemID";
    if ($conn->query($sql4) === TRUE) {
        header("Location: https://grocer.bot.nu");
        //header("Location: http://localhost/public_html/index.php");
    } else {
        echo "Error: " . $sql4 . "<br>" . $conn->error;
    }
}

// Updates the quantity of an item in list
if (isset($_POST['qty_up']) || isset($_POST['qty_down'])) {
    if (isset($_POST['qty_up'])) {
        $mappedItemID = $_POST['qty_up'];
    } else {
        $mappedItemID = $_POST['qty_down'];
    }

    $sql10 = "SELECT quantity FROM item_to_glist WHERE id = $mappedItemID";
    $result = $conn->query($sql10);
    
    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc(); 
        $qty = $row['quantity'];
        
        if (isset($_POST['qty_up'])) {
            $mappedItemID = $_POST['qty_up'];
            $qty = (int)$qty + 1;
            $sql9 = "UPDATE item_to_glist SET quantity = $qty WHERE id=$mappedItemID"; 
        } else {
            $mappedItemID = $_POST['qty_down'];
            $qty = (int)$qty - 1;
            $sql9 = "UPDATE item_to_glist SET quantity = $qty WHERE id=$mappedItemID";        
        }
        
        if ($conn->query($sql9) === TRUE) {
            header("Location: https://grocer.bot.nu");
            //header("Location: http://localhost/public_html/index.php");
        } else {
            echo "Error: " . $sql9 . "<br>" . $conn->error;
        }
    }
}


// Maps a food item to a users grocery list
if (isset($_POST['add_mapped_item'])) {
    $itemId = $_POST['item_id'];
    $glistId = $_POST['glist_id'];
    $itemQty = $_POST['item_qty'];
    $sql5 = "SELECT * FROM item WHERE item_id = $itemId";
    $result = $conn->query($sql5);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $photoUrl = $row['photo_url'];
        $itemName = $row['name'];
        $price = $row['price'];

        $sql6 = "INSERT INTO item_to_glist (item_id, glist_id, quantity) VALUES ($itemId, $glistId, $itemQty)";
        $conn->query($sql6);
        header("Location: https://grocer.bot.nu");
        //header("Location: http://localhost/public_html/index.php");
    } else {
        echo "Item with id $itemId does not exist!";
    }
}

// Deletes a food item from the database
if (isset($_POST['remove_item'])) {
    $itemId = $_POST['item_id'];
    $sql7 = "DELETE FROM item WHERE item_id = $itemId";
    if ($conn->query($sql7) === TRUE) {
        header("Location: https://grocer.bot.nu/catalog.php");
        //header("Location: http://localhost/public_html/catalog.php");
    } else {
        echo "Item with id $itemId does not exist!";
    }
}

// Adds an item to the catalog
if (isset($_POST['add_item'])) {
    $itemName = $_POST['item_name'];
    $itemImgUrl = $_POST['item_image_url'];
    $itemPrice = $_POST['item_price'];
    $sql8 = "INSERT INTO item (name, photo_url, price) VALUES ('$itemName', '$itemImgUrl', '$itemPrice')";
    if ($conn->query($sql8) === TRUE) {
        header("Location: https://grocer.bot.nu/catalog.php");
        //header("Location: http://localhost/public_html/catalog.php");
    } else {
        echo "Failure to add item!";
    }
}
?>
