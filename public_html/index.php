<?php
// Redirect user to login page if not currently logged in
session_start();
if (!isset($_SESSION['logged_in']) || !($_SESSION['logged_in'])) {
    header("Location: https://grocer.bot.nu/login.php");
}

require_once("../resources/config.php");
?>

<!--
     Still need to add
   - Share list
   - List stats (eg. total cost)
-->

<html>
    <head>
	    <script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js'></script>
  		<script type='text/javascript' src='js/functions.js'></script>
  		<script type='text/javascript' src='js/logout_post.js'></script>
      <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <!-- <link href="css/loginStyle.css" rel="stylesheet" /> -->
      <link href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" rel="stylesheet">
		<title>GrocerBot</title>
    </head>
    <body>
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark ustify-content-between">
        <a class="navbar-brand" href="#">GrocerBot</a>
        <div class="input-group">
          <form class="form-inline ml-auto mr-auto" id='add_glist' action='modify_glist.php' method='post'>
            <div class="input-group">
              <input class="form-control" type='text' name='glist_name' placeholder='new list name'>
              <div class="input-group-append">
                <button class="btn btn-success" type='submit'><i class="fas fa-cart-plus"></i></button>
              </div>
            </div>

          </form>
          <div class="input-group-append">

          </div>

        </div>

        <form class="form-inline ml-auto" id="logout_submit" action="./logout.php" method="post">
           <button class=" btn btn-danger"
                   type="submit" name="logout_btn" value="logout">Logout</button>
        </form>
      </nav>
      <div align='center'>


    </body>
		<?php
		// Create connection
		$conn = new mysqli($config['host'], $config['username'], $config['password'], $config['dbname']);

		// Check connection
		if ($conn->connect_error) {
				die("Connection failed: " . $conn->connect_error);
		}

		$userEmail = $_SESSION['email'];

		$sql = "SELECT name, list_id FROM glist JOIN (SELECT list_id FROM user_to_glist utg WHERE utg.user_id = (SELECT user.user_id FROM user WHERE email = '$userEmail')) t1 ON glist.glist_id = t1.list_id";
		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
			// Generate grocery list select list
			//echo "<form id='glist' action='modify_glist.php' method='post'>";





			//echo "</form>";

      echo "<div class=\"col-lg-6 col-md-8 col-sm-12\">";
      echo "<table class=\"table table-striped table-bordered\">
      <thead>";
      echo "<form id='glist' action='modify_glist.php' method='post'>";
       echo "<tr>
        <th style=\"width:75%;\">";
        echo "<div class=\"input-group mb-3\">";
        echo "<select class=\"custom-select\" id='selectGlist' onchange='updateSelectedGlist()' name='glist_sel'>";
          while ($row = $result -> fetch_assoc()) {
            $currentListName = $row['name'];
            $currentListID = $row['list_id'];
            echo "<option value=$currentListID>$currentListName</option>";
          }
        echo "</select>";
        echo "<div class=\"input-group-append\">
  <button class=\"btn btn-danger\" type=\"submit\" name='del_glist'><i class=\"fas fa-trash\"></i></button>
</div>";
        //echo "<input type='submit' name='del_glist' value='delete selected'>";

        echo "</div>";
        echo "</th>";


        echo "
      </tr>
      </thead>
      </table>";
      echo "</div>";
      echo "</form>";
      echo "<div id='list'></div>";

      echo "<form action='modify_glist.php' method='post'>";
      echo "<div class=\"input-group col-lg-6 col-md-8 col-sm-12\">";
        echo  "<input class=\"form-control\" type='text' name='item_id' placeholder='item id'>";
        echo "<input class=\"form-control\" type='text' name='item_qty' placeholder='quantity'>";
        echo  "<input type='hidden' id='glist_hidden' name='glist_id'>";
        echo "<div class=\"input-group-append\">";
      echo  "<button class=\"btn btn-success\" name='add_mapped_item' type='submit' >add item</button>";
      echo "</div>";
      echo "</div>";
      echo "</form>";

      //
      echo "<a href=\"catalog.php\">View Item IDs</a>";
      //
      echo "</div>";
		}
		// Reach this case if the user has no grocery lists
		else {
			echo "<p>No grocery lists currently found for user</p>";
		}

		$conn->close();
		?>

	<script type="text/javascript">
    // Call once to initialize selected grocery list
		updateSelectedGlist();
	</script>

</html>
