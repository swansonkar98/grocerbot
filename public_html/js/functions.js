function validateForm() {
    var price = document.forms["catalogalter"]["item_price"].value;
    if (isNaN(price)) {
        alert("Price must be numeric!");
        return false;
    }
}

function updateSelectedGlist() {
    var selectedName = document.getElementById("selectGlist").value;
    // Set the hidden field on item creation to the glist id
    $('#glist_hidden').val(selectedName);
    $.ajax({
        type: "POST",
        url: "food_table.php",
        data: { selectedGlist: selectedName },
        success: function (result) {
            console.log(result);
            $('#list').html(result);
        }
    });
}
