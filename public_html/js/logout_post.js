$(document).ready(function () {
    $('#logout_submit').submit(function () {
        var formdata = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "logout.php",
            data: formdata,
        });
        location.reload();
        return false;
    });
});