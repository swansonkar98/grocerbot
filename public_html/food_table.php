<?php
session_start();

if (!isset($_SESSION['logged_in']) || !($_SESSION['logged_in'])) {
    header("Location: https://grocer.bot.nu/login.php");
}

require_once("../resources/config.php");

// Create connection
$conn = new mysqli($config['host'], $config['username'], $config['password'], $config['dbname']);

// Check connection
if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
}

$userEmail = $_SESSION['email'];

if (isset($_POST['selectedGlist'])) {
    $selectedGlist = $_POST['selectedGlist'];
    //echo $selectedGlist;
    $sql = "SELECT id, name, photo_url, price, quantity FROM (SELECT id, name, photo_url, price, quantity FROM item JOIN (SELECT item_id, id, quantity FROM item_to_glist WHERE glist_id = $selectedGlist) t1 ON item.item_id = t1.item_id) t2";
    $result = $conn->query($sql);
//style=\"width:50%;\"
    echo "<div class=\"col-lg-6 col-md-8 col-sm-12\">";
    echo "<form id='foodremove' action='modify_glist.php' method='post'>";
    echo "<table class=\"table table-striped table-bordered\">";

    echo "<thead class=\"thead-dark\">

      <tr>
        <th>&nbsp</th>
        <th>Image</th>
        <th>Name</th>
        <th>Quantity</th>
        <th>Price</th>
        <th>&nbsp</th>
      </tr>
    </thead>";

    if ($result->num_rows > 0) {
	$totalSum = 0;
        while ($row = $result -> fetch_assoc()) {
            $itemMappedId = $row['id'];
            $foodName = $row['name'];
            $foodImgUrl = $row['photo_url'];
            $foodQty = $row['quantity'];
            $foodPrice = (float)$row['price'] * (float)$foodQty;
            echo "<tr>";
            echo "<td><div class=\"form-check\"><input class=\"form-check-input\" type=\"checkbox\"></div></td>";
            echo "<td><img width='100' height='100' src='$foodImgUrl'></td><td>$foodName</td>";
	    echo "<td><button name='qty_down' type='submit' value='$itemMappedId'>-</button>";
            echo "  $foodQty  ";
	    echo "<button name='qty_up' type='submit' value='$itemMappedId'>+</button></td>";
            echo "<td>$foodPrice</td>";
            echo "<td><button class=\"btn btn-danger\" type='submit' name='remove_mapped_item' value='$itemMappedId'><i class=\"fas fa-minus\"></i></button></td>";
            echo "</tr>";
	    $totalSum += $foodPrice;
        }
    }
    echo "<tr><td></td><td></td><td></td><td>Total</td><td>$totalSum</td><td></td></tr>";
    echo "</table>";

    echo "</form>";
    echo "</div>";
}
?>
