<?php
session_start();
require_once("../resources/config.php");
?>

<html>
  <head>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="css/loginStyle.css" rel="stylesheet" />
    <link href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" rel="stylesheet">
  </head>
  <body>
    <div class="container">
      <div class="login_pane">
        <div id="login-content-wrapper">
          <h1 id="login-header">GrocerBot</h1>
          <form method="post" action="login.php"><br>
            <div class="input-group">
		          <div class="input-group-prepend">
                <span class="input-group-text"><i class="far fa-envelope"></i></span>
		          </div>
		          <input class="form-control" type="text" name="email" placeholder="Enter Email">
            </div>
            <br />
            <div class="input-group">
		          <div class="input-group-prepend">
                <span class="input-group-text"><i class="fas fa-key"></i></span>
		          </div>
		          <input class="form-control" type="password" name="password" placeholder="Enter Password"><br>
            </div>
            <br />
            <button class="btn btn-success btn-block" type="submit" value="submit">Login</button>
          </form>
          <a class="btn btn-primary btn-block" href="register.php">Sign up</a>

          <?php
          ini_set('display_errors', 1);
          ini_set('display_startup_errors', 1);
          error_reporting(E_ALL);

          if(isset($_SESSION['logged_in'])) {
              echo $_SESSION['email'];
          }

          if (!empty($_POST))
          {
              $email = $_POST["email"];
              $user_password = $_POST["password"];

            // Create connection
            $conn = new mysqli($config['host'], $config['username'], $config['password'], $config['dbname']);

            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT password FROM user WHERE email='$email'";
            $result = $conn->query($sql);
            echo empty($result);
            if($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    $hash = $row['password'];
                    if (password_verify($user_password, $hash)) {
                        echo "Logged in!";
                        // Logs the user into a session
                        $_SESSION['logged_in'] = True;
                        $_SESSION['email'] = $email;
                        header('Location: index.php');
                        die();
                    } else {
                    echo "<p class='login_error_text'>Incorrect password!</p>";
                    }
                }
              } else {
                echo "<p class='login_error_text'>Email invalid!</p>";
              }

              $conn->close();
          }
          ?>
        </div>
      </div>
    </div>

  </body>
</html>
