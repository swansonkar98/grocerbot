<html>
<a href="index.php">back to home</a>
    <head>
        <script type='text/javascript' src='js/functions.js'></script>
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <?php
    session_start();
    require_once("../resources/config.php");

    // Create connection
    $conn = new mysqli($config['host'], $config['username'], $config['password'], $config['dbname']);

    // Check connection
    if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
    }

    // Branch here for user and admin roles
    $email = $_SESSION['email'];
    $sql = "SELECT role FROM user WHERE email = '$email'";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        $row = $result->fetch_assoc();
        $role = $row['role'];
    }

    $sql1 = "SELECT * FROM item";
    $result1 = $conn->query($sql1);

    if ($role == 'admin') {
        $sql1 = "SELECT * FROM item";
        $result1 = $conn->query($sql1);

        echo "<form name='catalogalter' action='modify_glist.php' method='post' onsubmit='return validateForm()'>";
        echo "<table class=\"table table-striped table-bordered\">";
        echo "<thead>";

        echo "<tr><td>Image</td><td>ID</td><td>Name</td><td>Price</td><td>Delete</td></tr>";
        echo "</thead>";
        echo "<tbody>";


        if ($result1->num_rows > 0) {
            while ($row = $result1->fetch_assoc()) {
                $itemId = $row['item_id'];
                $foodName = $row['name'];
                $foodImgUrl = $row['photo_url'];
                $foodPrice = $row['price'];
                echo "<tr>
                <td>$foodImgUrl</td>
                <td>$itemId</td>
                <td>$foodName</td>
                <td>$foodPrice</td>
                <td>
                <input name='item_id' value='$itemId' type='hidden'>
                <button class=\"btn btn-danger\" type='submit' name='remove_item' value='-'>delete</button>
                </td></tr>";
            }
        }

        echo "<div class=\"input-group\">";
        echo "<input class=\"form-control\" name='item_name' type='text' placeholder='item name'>";
        echo "<input class=\"form-control\" name='item_image_url' type='text' placeholder='item image url'>";
        echo "<input class=\"form-control\" name='item_price' type='text' placeholder='item price'>";
        echo "<button class=\"btn btn-success\" name='add_item' type='submit' value='Add Item'>Add Item</button>";
        echo "</div>";

        echo "</tbody>";
        echo "</table>";
        echo "</form>";

    } else if ($role == 'user') {
        echo "<table class=\"table table-striped table-bordered\">>";
        echo "<tr><td>Image</td><td>ID</td><td>Name</td><td>Price</td></tr>";
        if ($result1->num_rows > 0) {
            while ($row = $result1->fetch_assoc()) {
                $itemId = $row['item_id'];
                $foodName = $row['name'];
                $foodImgUrl = $row['photo_url'];
                $foodPrice = $row['price'];
                echo "<tr><td>$foodImgUrl</td><td>$itemId</td><td>$foodName</td><td>$foodPrice</td></tr>";
            }
        }
        echo "</table>";
    }
    ?>
</html>
