<?php
  session_start();
  require_once("../resources/config.php");
?>

<html>
<head>
  <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link href="css/registerStyle.css" rel="stylesheet" />
  <link href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" rel="stylesheet">
</head>
<body>
  <div class="container">
    <div id="register-pane">
      <div id="register-content-wrapper">
        <h1>Register</h1>
        <form method="post" action="register.php">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="far fa-envelope"></i></span>
            </div>
            <input class="form-control" type="text" name="email" placeholder="Enter Email">
          </div>
          <br />
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="fas fa-key"></i></span>
            </div>
            <input class="form-control" type="password" name="password" placeholder="Enter Password"><br>
          </div>
          <br />
          <br />
          <button class="btn btn-success btn-block" type="submit" value="submit">Sign up</button>
        </form>
      </div>
    </div>
  </div>
</body>
</html>

<?php
if (!empty($_POST)) {
    $email = $_POST["email"];

    // Validate email address
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      echo "Invalid email address!";
      die();
    }

    $user_password = $_POST["password"];
    $hash = password_hash($user_password, PASSWORD_DEFAULT);

    // Create connection

    $conn = new mysqli($config['host'], $config['username'], $config['password'], $config['dbname']);

    // Check connection
    if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
    }

    // Check that email is not already in use
    $sql1 = "SELECT user_id FROM user WHERE email='$email'";
    $result = $conn->query($sql1);
    if($result->num_rows > 0) {
	    echo "Email already in use!";
    } else {
	    $sql2 = "INSERT INTO user (email, password) VALUES ('$email', '$hash')";
	    if ($conn->query($sql2) === TRUE) {
        echo "Successful registration, you may now log in";
        // Autologin here, redirects to index.php
        $_SESSION['email'] = $email;
        $_SESSION['logged_in'] = True;
        $conn->close();
        header('Location: index.php');
	    } else {
	    echo "Error: " . $sql2 . "<br>" . $conn->error;
	  }
  }

    $conn->close();
}
?>
