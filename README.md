# GrocerBot
> A grocery list application making it easier to manage groceries, helps in
deciding what groceries to buy based on total cost.

> https://grocer.bot.nu

Currently GrocerBot features the following functionality
* User registration
* User authentication and cookie assignment
* Password hashing
* Creating grocery lists
* Removing grocery lists
* Adding to grocery lists
* Removing from grocery lists
* Modifying quantity of items in grocery lists
* Adding to the item catalog (ADMIN ONLY)
* Removing from the item catalog (ADMIN ONLY)
* Item image rendering in list
* Item cost totaling for lists
* SSL Certificate through certbot
* Qualified subdomain name

## Requirements and Installation
This application was developed on an AWS instance running Ubuntu 18.04 LTS and
has the following dependencies.
* Apache2
* PHP7.0
* MySQL 5.7.26

### Installation
First use the schema provided in /resources/grocerbot.sql to create the
required MySQL database and tables

#### Ubuntu 18.04
The repository should be cloned to /var/www/

The site below gives an excellent list of the steps require to install a LAMP
server, which is essentially what we are running: https://medium.com/@jangid.hitesh2112/how-to-install-lamp-stack-on-ubuntu-db77ac018116

Update the repository
```
sudo apt-get update
```
Install MySQL
```
sudo apt-get install mysql-server mysql-client libmysqlclient-dev
```
Install Apache webserver
```
sudo apt-get install apache2 apache2-doc apache2-npm-prefork apache2-utils libexpat1 ssl-cert
```
Install PHP7.0
```
sudo apt-get install libapache2-mod-php7.0 php7.0 php7.0-common php7.0-curl php7.0-dev php7.0-gd php-pear php-imagick php7.0-mcrypt php7.0-mysql php7.0-ps php7.0-xsl
```
Install Phpmyadmin
```
sudo apt-get install phpmyadmin
```

To forward a subdomain to the files, please follow the example apache configs
found in /resources/apache_conf/ we included two for SSL and non-SSL 
configurations. These configs should be placed in /etc/apache2/sites-available
and properly enabled. NOTE if using SSL you will need a certificate, we use
https://certbot.eff.org/ and highly recommend the service!

#### Windows 10
We do not recommend running this on Windows, please instead visit 
https://grocer.bot.nu for a live demo

To install GrocerBot on Windows you must first install a PHP interpreter, and
modify our config.php file located in /resources/config.php so that the MySQL
connection details correspond with your MySQL instance

## Future Functionality
If we continue this project in the future, we intend to add functionality supporting
* Sharing grocery lists
* Automatically pulling grocery item information from Amazon's API
* Better image rendering
* Live updates on sharing

## Authors
Karl Swanson

Mikhail Dorokhov
    

