USE grocerbot

CREATE TABLE user (
       user_id		INT		AUTO_INCREMENT		PRIMARY KEY,
       email		VARCHAR(256)	NOT NULL,
       password		VARCHAR(256)	NOT NULL,
       role             VARCHAR(256)  DEFAULT 'user'
);

CREATE TABLE glist (
       glist_id		INT		AUTO_INCREMENT		PRIMARY KEY,
       name		VARCHAR(256)	NOT NULL
);

CREATE TABLE user_to_glist (
       id    		INT		AUTO_INCREMENT		PRIMARY KEY,
       user_id		INT		NOT NULL,
       list_id		INT		NOT NULL,
       CONSTRAINT lu_user_id_fk
              FOREIGN KEY (user_id) REFERENCES user(user_id),
       CONSTRAINT lu_list_id_fk
              FOREIGN KEY (list_id) REFERENCES glist(glist_id) ON DELETE CASCADE
);

CREATE TABLE item (
       item_id		INT		AUTO_INCREMENT		PRIMARY KEY,
       photo_url	VARCHAR(256),
       name		VARCHAR(256)	NOT NULL,
       price		DECIMAL(9,2)
);

CREATE TABLE item_to_glist (
       id		INT		AUTO_INCREMENT		PRIMARY KEY,
       item_id		INT		NOT NULL,
       glist_id		INT		NOT NULL,
       quantity		INT		NOT NULL
       CONSTRAINT ig_item_id_fk
              FOREIGN KEY (item_id) REFERENCES item(item_id) ON DELETE CASCADE,
       CONSTRAINT ig_glist_id_fk
              FOREIGN KEY (glist_id) REFERENCES glist(glist_id) ON DELETE CASCADE       
);

CREATE TABLE registration_log (
       id		 INT		AUTO_INCREMENT		PRIMARY KEY,
       timestamp	 TIMESTAMP,
       user_id		 INT		NOT NULL,
       email		 VARCHAR(256)	NOT NULL,
       CONSTRAINT rl_user_id_fk
              FOREIGN KEY (user_id) REFERENCES user(user_id)     
);

DELIMITER //
CREATE TRIGGER update_reg_log
AFTER INSERT ON user FOR EACH ROW
BEGIN
INSERT INTO registration_log (user_id, email) VALUES (NEW.user_id, NEW.email);
END; //
DELIMITER ;
